<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?= $this->getContent() ?>

<div align="rigth">
    <?= $this->tag->linkTo(['directores/new', 'Guardar Informacion de Nuevo Director', 'class' => 'btn btn-primary']) ?>
</div>
<?= $this->tag->form(['directores/search', 'autocomplete' => 'off']) ?>
<br><br>
<div class="center scaffold">

    <h2>Buscar Informacion de Director</h2>

    <div class="clearfix">
        <label for="numid">Numero de Identificacion</label>
        <?= $this->tag->numericField(['numid', 'size' => 10, 'maxlength' => 10]) ?>
    </div>

    <div class="clearfix">
        <label for="name">Nombre</label>
        <?= $this->tag->textField(['name', 'size' => 24, 'maxlength' => 70]) ?>
    </div>

    <div class="clearfix">
        <label for="lastname">Apellido</label>
        <?= $this->tag->textField(['lastname', 'size' => 24, 'maxlength' => 70]) ?>
    </div>

    <div class="clearfix">
        <?= $this->tag->submitButton(['Buscar', 'class' => 'btn btn-primary']) ?>
    </div>

</div>


</form>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>
