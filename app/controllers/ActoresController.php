<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class ActoresController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for actores
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Actores', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $actores = Actores::find($parameters);
        if (count($actores) == 0) {
            $this->flash->notice("Esta busqueda no Produjo resultados");

            $this->dispatcher->forward([
                "controller" => "actores",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $actores,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a actore
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $actore = Actores::findFirstByid($id);
            if (!$actore) {
                $this->flash->error("Actor no Encontrado");

                $this->dispatcher->forward([
                    'controller' => "actores",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $actore->id;

            $this->tag->setDefault("id", $actore->id);
            $this->tag->setDefault("numid", $actore->numid);
            $this->tag->setDefault("name", $actore->name);
            $this->tag->setDefault("lastname", $actore->lastname);
            
        }
    }

    /**
     * Creates a new actore
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'index'
            ]);

            return;
        }

        $actore = new Actores();
        $actore->numid = $this->request->getPost("numid");
        $actore->name = $this->request->getPost("name");
        $actore->lastname = $this->request->getPost("lastname");
        

        if (!$actore->save()) {
            foreach ($actore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Actor guardado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "actores",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a actore edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $actore = Actores::findFirstByid($id);

        if (!$actore) {
            $this->flash->error("Ese actor no existe " . $id);

            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'index'
            ]);

            return;
        }

        $actore->numid = $this->request->getPost("numid");
        $actore->name = $this->request->getPost("name");
        $actore->lastname = $this->request->getPost("lastname");
        

        if (!$actore->save()) {

            foreach ($actore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'edit',
                'params' => [$actore->id]
            ]);

            return;
        }

        $this->flash->success("Actor actualizao satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "actores",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a actore
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $actore = Actores::findFirstByid($id);
        if (!$actore) {
            $this->flash->error("Actor no Encontrado");

            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'index'
            ]);

            return;
        }

        if (!$actore->delete()) {

            foreach ($actore->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "actores",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Actor Eliminado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "actores",
            'action' => "index"
        ]);
    }

}
