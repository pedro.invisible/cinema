<html>
 <head>
  <title>Bienvenido a Cinema</title>
  {{ stylesheet_link('css/bootstrap.theme.min.css') }}
  {{ javascript_include('js/bootstrap.min.js') }}
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px">{{ link_to("index", "Home", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("peliculas", "Peliculas", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("actores", "Actores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("directores", "Directores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("casting", "Casting", "class": "btn btn-primary") }}</li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
{{ content() }}

<ul class="pager">
    <li class="btn btn-success">
        {{ link_to("directores", "&larr; Volver", "class": "btn btn-success") }}
    </li>
    <li class="btn btn-success">
        {{ link_to("directores/new", "Guardar informacion de un Nuevo Director", "class": "btn btn-success") }}
    </li>
</ul>
<br>
{% for directores in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Numero de Identificacion</th>
            <th>Nombre</th>
            <th>Apellido</th>
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ directores.numid }}</td>
            <td>{{ directores.name }}</td>
            <td>{{ directores.lastname }}</td>
            <?php echo $this->tag->hiddenField("directores.id") ?>
            <td width="7%">{{ link_to("directores/edit/" ~ directores.id, '<i class="glyphicon glyphicon-edit"></i> Editar', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("directores/delete/" ~ directores.id, '<i class="glyphicon glyphicon-remove"></i> Eliminar', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="4" align="right">
                <div class="btn-group">
                    {{ link_to("directores/search", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("directores/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("directores/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("directores/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
    {% endif %}
{% else %}
    No se ha guardado ningun director
{% endfor %}
</form>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>