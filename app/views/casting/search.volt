<html>
 <head>
  <title>Bienvenido a Cinema</title>
  {{ stylesheet_link('css/bootstrap.theme.min.css') }}
  {{ javascript_include('js/bootstrap.min.js') }}
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px">{{ link_to("index", "Home", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("peliculas", "Peliculas", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("actores", "Actores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("directores", "Directores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("casting", "Casting", "class": "btn btn-primary") }}</li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
{{ content() }}

<div align="rigth">
    {{ link_to("casting/new", "Casting Nuevo", "class": "btn btn-primary") }}

    {{ link_to("casting", "Volver", "class": "btn btn-primary") }}
</div>
<br>
{% for casting in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Pelicula</th>
            <th>Actor</th>
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ casting.getPeliculas().nombre }}</td>
            <td>{{ casting.getActores().name }} {{ casting.getActores().lastname }}</td>
            <td width="7%">{{ link_to("casting/edit/" ~ casting.id, '<i class="glyphicon glyphicon-edit"></i> Editar', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("casting/delete/" ~ casting.id, '<i class="glyphicon glyphicon-remove"></i> Eliminar', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="4" align="right">
                <div class="btn-group">
                    {{ link_to("casting/search", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("casting/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("casting/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("casting/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
    {% endif %}
{% else %}
    No se ha guardado el casting
{% endfor %}
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estas y mucho mas! </p>
            </footer>
        </div>
    </div>
</div>
