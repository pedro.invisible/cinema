<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class DirectoresController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }


    /**
     * Searches for directores
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Directores', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $directores = Directores::find($parameters);
        if (count($directores) == 0) {
            $this->flash->notice("Esta busqueda no Produjo resultados");

            $this->dispatcher->forward([
                "controller" => "directores",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $directores,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a Director
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $directores = Directores::findFirstByid($id);
            if (!$directores) {
                $this->flash->error("Director no Encontrado");

                $this->dispatcher->forward([
                    'controller' => "Directores",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $directores->id;

            $this->tag->setDefault("id", $directores->id);
            $this->tag->setDefault("numid", $directores->numid);
            $this->tag->setDefault("name", $directores->name);
            $this->tag->setDefault("lastname", $directores->lastname);
            
        }
    }

    /**
     * Creates a new director
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'index'
            ]);

            return;
        }

        $directores = new Directores();
        $directores->numid = $this->request->getPost("numid");
        $directores->name = $this->request->getPost("name");
        $directores->lastname = $this->request->getPost("lastname");
        

        if (!$directores->save()) {
            foreach ($directores->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Director guardado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "directores",
            'action' => 'index'
        ]);
    }

    /**
     * Saves directores edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $directores = Directores::findFirstByid($id);

        if (!$directores) {
            $this->flash->error("Ese director no existe " . $id);

            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'index'
            ]);

            return;
        }

        $directores->numid = $this->request->getPost("numid");
        $directores->name = $this->request->getPost("name");
        $directores->lastname = $this->request->getPost("lastname");
        

        if (!$directores->save()) {

            foreach ($directores->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'edit',
                'params' => [$directores->id]
            ]);

            return;
        }

        $this->flash->success("Director actualizao satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "directores",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes directores
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $directores = Directores::findFirstByid($id);
        if (!$directores) {
            $this->flash->error("Director no Encontrado");

            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'index'
            ]);

            return;
        }

        if (!$directores->delete()) {

            foreach ($directores->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "directores",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Director Eliminado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "directores",
            'action' => "index"
        ]);
    }

}
