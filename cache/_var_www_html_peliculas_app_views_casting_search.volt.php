<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?= $this->getContent() ?>

<div align="rigth">
    <?= $this->tag->linkTo(['casting/new', 'Casting Nuevo', 'class' => 'btn btn-primary']) ?>

    <?= $this->tag->linkTo(['casting', 'Volver', 'class' => 'btn btn-primary']) ?>
</div>
<br>
<?php $v116128105124968747231iterated = false; ?><?php $v116128105124968747231iterator = $page->items; $v116128105124968747231incr = 0; $v116128105124968747231loop = new stdClass(); $v116128105124968747231loop->self = &$v116128105124968747231loop; $v116128105124968747231loop->length = count($v116128105124968747231iterator); $v116128105124968747231loop->index = 1; $v116128105124968747231loop->index0 = 1; $v116128105124968747231loop->revindex = $v116128105124968747231loop->length; $v116128105124968747231loop->revindex0 = $v116128105124968747231loop->length - 1; ?><?php foreach ($v116128105124968747231iterator as $casting) { ?><?php $v116128105124968747231loop->first = ($v116128105124968747231incr == 0); $v116128105124968747231loop->index = $v116128105124968747231incr + 1; $v116128105124968747231loop->index0 = $v116128105124968747231incr; $v116128105124968747231loop->revindex = $v116128105124968747231loop->length - $v116128105124968747231incr; $v116128105124968747231loop->revindex0 = $v116128105124968747231loop->length - ($v116128105124968747231incr + 1); $v116128105124968747231loop->last = ($v116128105124968747231incr == ($v116128105124968747231loop->length - 1)); ?><?php $v116128105124968747231iterated = true; ?>
    <?php if ($v116128105124968747231loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Pelicula</th>
            <th>Actor</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?= $casting->getPeliculas()->nombre ?></td>
            <td><?= $casting->getActores()->name ?> <?= $casting->getActores()->lastname ?></td>
            <td width="7%"><?= $this->tag->linkTo(['casting/edit/' . $casting->id, '<i class="glyphicon glyphicon-edit"></i> Editar', 'class' => 'btn btn-default']) ?></td>
            <td width="7%"><?= $this->tag->linkTo(['casting/delete/' . $casting->id, '<i class="glyphicon glyphicon-remove"></i> Eliminar', 'class' => 'btn btn-default']) ?></td>
        </tr>
    <?php if ($v116128105124968747231loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="4" align="right">
                <div class="btn-group">
                    <?= $this->tag->linkTo(['casting/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['casting/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['casting/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['casting/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn']) ?>
                    <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
    <?php } ?>
<?php $v116128105124968747231incr++; } if (!$v116128105124968747231iterated) { ?>
    No se ha guardado el casting
<?php } ?>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estas y mucho mas! </p>
            </footer>
        </div>
    </div>
</div>
