<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class CastingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $this->view->peliculas = Peliculas::find();
        $this->view->actores = Actores::find();
    }

    /**
     * Searches for Casting
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Casting', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $casting = Casting::find($parameters);
        if (count($casting) == 0) {
            $this->flash->notice("Esta busqueda no Produjo resultados");

            $this->dispatcher->forward([
                "controller" => "Casting",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $casting,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
        $this->view->peliculas = Peliculas::find();       
        $this->view->actores = Actores::find();
    }

    public function peliculasAction()
    {
       $peliculas = Peliculas::findFirstByid($id);

// Creando una etiqueta Select con una opción vacía y un texto por defecto
    echo $this->tag->select
        (
        [
        'idpelicula',
        $peliculas,
        'using'      => [
            'id',
            'nombre',
        ],
        'useEmpty'   => true,
        'emptyText'  => 'Por favor, seleccionar una opción...',
        'emptyValue' => '',
        ]
        );


       $this->view->peliculas = Peliculas::find();
    }


public function actoresAction()
    {
       $actores = Actores::findFirstByid($id);

// Creando una etiqueta Select con una opción vacía y un texto por defecto
  /*
    echo $this->tag->select
        (
        [
        'idactor',
        $actores,
        'using'      => [
            'id',
            'name',
            'lastname',
        ],
        'useEmpty'   => true,
        'emptyText'  => 'Por favor, seleccionar una opción...',
        'emptyValue' => '',
        ]
        );

*/
        $phql = "SELECT CONCAT(a.name, ' ', a.lastname) AS fullname FROM Actores AS a ORDER BY a.name";
        $full = $manager->executeQuery($phql);
        foreach ($full as $fullnames) 
            {
            echo $fullnames->fullname, "\n";
            }

        echo Phalcon\Tag::select(
                            array(
                                'idactor',
                                Actores::find(array(
                                            'order'=>'name', 
                                            'columns'=> array('id', ' CONCAT(name, lastname) as fullname'),
                                            )),
                                    'using' => array('id', 'fullname'),
                                    'class' => 'form-control input-medium',
                                    'emptyText' => 'Produccion...'
                            )
                        );

       $this->view->actores = Actores::find();
    }


    /**
     * Displays the creation form
     */
    public function newAction()
    {
    
    $this->view->actores = Actores::find();
    $this->view->peliculas = Peliculas::find();
    
    }


    /**
     * Edits a Director
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $casting = Casting::findFirstByid($id);
            if (!$casting) {
                $this->flash->error("Casting no Encontrado");

                $this->dispatcher->forward([
                    'controller' => "Casting",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $casting->id;

            $this->tag->setDefault("id", $casting->id);
            $this->tag->setDefault("idpelicula", $casting->idpelicula);
            $this->tag->setDefault("idactor", $casting->idactor);
            $this->view->peliculas = Peliculas::find();       
            $this->view->actores = Actores::find();
        }
    }

    /**
     * Creates a new director
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'index'
            ]);

            return;
        }

        $casting = new Casting();
        $casting->idpelicula = $this->request->getPost("idpelicula");
        $casting->idactor = $this->request->getPost("idactor");
        

        if (!$casting->save()) {
            foreach ($casting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Casting guardado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "casting",
            'action' => 'index'
        ]);
        $this->view->peliculas = Peliculas::find();
        $this->view->actores = Actores::find();
    }

    /**
     * Saves Casting edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $casting = Casting::findFirstByid($id);

        if (!$casting) {
            $this->flash->error("Ese casting no existe " . $id);

            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'index'
            ]);

            return;
        }

        $casting->idpelicula = $this->request->getPost("idpelicula");
        $casting->idactor = $this->request->getPost("idactor");
        

        if (!$casting->save()) {

            foreach ($casting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'edit',
                'params' => [$casting->id]
            ]);

            return;
        }

        $this->flash->success("Casting actualizao satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "casting",
            'action' => 'index'
        ]);
        $this->view->peliculas = Peliculas::find();
        $this->view->actores = Actores::find();
    }

    /**
     * Deletes Casting
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $casting = Casting::findFirstByid($id);
        if (!$casting) {
            $this->flash->error("Casting no Encontrado");

            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'index'
            ]);

            return;
        }

        if (!$casting->delete()) {

            foreach ($casting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "casting",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Casting Eliminado satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "casting",
            'action' => "index"
        ]);
    }

}

