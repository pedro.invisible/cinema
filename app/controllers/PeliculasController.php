<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class PeliculasController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
        $this->view->directores = Directores::find();
    }

    public function directoresAction()
    {
       $directores = Directores::findFirstByid($id);

// Creando una etiqueta Select con una opción vacía y un texto por defecto
    echo $this->tag->select
        (
        [
        'iddirector',
        $directores,
        'using'      => [
            'id',
            'name',
            'lastname',
        ],
        'useEmpty'   => true,
        'emptyText'  => 'Por favor, seleccionar una opción...',
        'emptyValue' => '',
        ]
        );


       $this->view->directores = Directores::find();
    }

    /**
     * Searches for actores
     */
    public function searchAction()
    {
       $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Peliculas", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $peliculas = Peliculas::find($parameters);
        if (count($peliculas) == 0) {
            $this->flash->notice("Esta busqueda no encontro ninguna peliculas");

            return $this->dispatcher->forward(
                [
                    "controller" => "peliculas",
                    "action"     => "index",
                ]
            );
        }

        $paginator = new Paginator(array(
            "data"  => $peliculas,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->directores = Directores::find();
    }

    /**
     * Shows the form to create a new peliculas
     */
    public function newAction()
    {
        $this->view->directores = Directores::find();
    }

    /**
     * Edits a peliculas based on its id
    *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $peliculas = Peliculas::findFirstByid($id);
            if (!$peliculas) {
                $this->flash->error("Pelicula no Encontrada");

                $this->dispatcher->forward([
                    'controller' => "peliculas",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $peliculas->id;

            $this->tag->setDefault("id", $peliculas->id);
            $this->tag->setDefault("nombre", $peliculas->nombre);
            $this->tag->setDefault("iddirector", $peliculas->iddirector);
            $this->tag->setDefault("year", $peliculas->year);
            $this->view->directores = Directores::find();

        }
    }

    /**
     * Creates a new peliculas
     */
     public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "peliculas",
                'action' => 'index'
            ]);

            return;
        }

        $peliculas = new Peliculas();
        $peliculas->nombre = $this->request->getPost("nombre");
        $peliculas->iddirector = $this->request->getPost("iddirector");
        $peliculas->year = $this->request->getPost("year");
        

        if (!$peliculas->save()) {
            foreach ($peliculas->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "peliculas",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Pelicula guardada satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "peliculas",
            'action' => 'index'
        ]);
        $this->view->directores = Directores::find();

    }

    /**
     * Saves current peliculas in screen
     *
     * @param string $id
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "peliculas",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $peliculas = Peliculas::findFirstByid($id);

        if (!$peliculas) {
            $this->flash->error("Esa Pelicula no existe " . $id);

            $this->dispatcher->forward([
                'controller' => "peliculas",
                'action' => 'index'
            ]);

            return;
        }

        $peliculas->nombre = $this->request->getPost("nombre");
        $peliculas->iddirector = $this->request->getPost("iddirector");
        $peliculas->year = $this->request->getPost("year");
        

        if (!$peliculas->save()) {

            foreach ($peliculas->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "peliculas",
                'action' => 'edit',
                'params' => [$peliculas->id]
            ]);

            return;
        }

        $this->flash->success("Pelicula actualizada satisfactoriamente");

        $this->dispatcher->forward([
            'controller' => "peliculas",
            'action' => 'index'
        ]);

        $this->view->directores = Directores::find();

    }
    /**
     * Deletes a peliculas
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $peliculas = Peliculas::findFirstById($id);
        if (!$peliculas) {
            $this->flash->error("peliculas was not found");

            return $this->dispatcher->forward(
                [
                    "controller" => "peliculas",
                    "action"     => "index",
                ]
            );
        }

        if (!$peliculas->delete()) {
            foreach ($peliculas->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "peliculas",
                    "action"     => "search",
                ]
            );
        }

        $this->flash->success("peliculas was deleted");

            return $this->dispatcher->forward(
                [
                    "controller" => "peliculas",
                    "action"     => "index",
                ]
            );
    }


    
public function castAction($id)
    {
        
        $phql1 = "SELECT p.id, p.nombre, p.iddirector, p.year, d.id, d.name as nomd, d.lastname as aped 
        FROM Peliculas p, Directores d 
        WHERE p.id = '$id' AND p.iddirector = d.id";
        $cast1 = $this->modelsManager->executeQuery($phql1);
    
        foreach ($cast1 as $peli) 
            {
               echo"  <table class='table table-bordered table-striped' align='center'>
                        <thead>
                        <tr>
                        <th>Pelicula</th>
                        <th>Año de Estreno</th>
                        <th>Director</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        <td>".$peli->nombre." </td>
                        <td>".$peli->year." </td>
                        <td>".$peli->nomd." ".$peli->aped."</td>
                        </tr>
                      </tbody>
                    </table>   ";


            }


        $phql = "SELECT p.id, c.idpelicula, c.idactor, a.id, a.name, a.lastname
        FROM Peliculas p, Casting c, Actores a
        WHERE p.id = '$id' AND c.idpelicula = p.id AND a.id = c.idactor";
        $cast = $this->modelsManager->executeQuery($phql);
 
            echo "<table class='table table-bordered table-striped' align='center'>
                    <thead>
                    <tr>
                    <th>Actores</th>
                    </tr>
                    </thead>";
              
                    foreach($cast as $castingfull)
                    {
                  echo "<tbody>
                        <tr>
                        <td>", $castingfull->name, " ",$castingfull->lastname," </td>
                        </tr>
                        </tbody>";
                    }

                echo "</table>";

    }

}



