<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?= $this->getContent() ?>

<ul class="pager">
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['directores', '&larr; Volver', 'class' => 'btn btn-success']) ?>
    </li>
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['directores/new', 'Guardar informacion de un Nuevo Director', 'class' => 'btn btn-success']) ?>
    </li>
</ul>
<br>
<?php $v156769815698663456141iterated = false; ?><?php $v156769815698663456141iterator = $page->items; $v156769815698663456141incr = 0; $v156769815698663456141loop = new stdClass(); $v156769815698663456141loop->self = &$v156769815698663456141loop; $v156769815698663456141loop->length = count($v156769815698663456141iterator); $v156769815698663456141loop->index = 1; $v156769815698663456141loop->index0 = 1; $v156769815698663456141loop->revindex = $v156769815698663456141loop->length; $v156769815698663456141loop->revindex0 = $v156769815698663456141loop->length - 1; ?><?php foreach ($v156769815698663456141iterator as $directores) { ?><?php $v156769815698663456141loop->first = ($v156769815698663456141incr == 0); $v156769815698663456141loop->index = $v156769815698663456141incr + 1; $v156769815698663456141loop->index0 = $v156769815698663456141incr; $v156769815698663456141loop->revindex = $v156769815698663456141loop->length - $v156769815698663456141incr; $v156769815698663456141loop->revindex0 = $v156769815698663456141loop->length - ($v156769815698663456141incr + 1); $v156769815698663456141loop->last = ($v156769815698663456141incr == ($v156769815698663456141loop->length - 1)); ?><?php $v156769815698663456141iterated = true; ?>
    <?php if ($v156769815698663456141loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Numero de Identificacion</th>
            <th>Nombre</th>
            <th>Apellido</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?= $directores->numid ?></td>
            <td><?= $directores->name ?></td>
            <td><?= $directores->lastname ?></td>
            <?php echo $this->tag->hiddenField("directores.id") ?>
            <td width="7%"><?= $this->tag->linkTo(['directores/edit/' . $directores->id, '<i class="glyphicon glyphicon-edit"></i> Editar', 'class' => 'btn btn-default']) ?></td>
            <td width="7%"><?= $this->tag->linkTo(['directores/delete/' . $directores->id, '<i class="glyphicon glyphicon-remove"></i> Eliminar', 'class' => 'btn btn-default']) ?></td>
        </tr>
    <?php if ($v156769815698663456141loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="4" align="right">
                <div class="btn-group">
                    <?= $this->tag->linkTo(['directores/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['directores/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['directores/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['directores/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn']) ?>
                    <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
    <?php } ?>
<?php $v156769815698663456141incr++; } if (!$v156769815698663456141iterated) { ?>
    No se ha guardado ningun director
<?php } ?>
</form>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>