<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div align="rigth">
    <?= $this->tag->linkTo(['casting/new', 'Casting Nuevo', 'class' => 'btn btn-primary']) ?>
</div>
<br>
<div align="rigth">
    <?= $this->tag->linkTo(['casting/search', 'Volver', 'class' => 'btn btn-primary']) ?>
</div>
<br>

<div class="page-header">
    <h1>
        Editar Informacion del Casting
    </h1>
</div>

<?php echo $this->getContent(); ?>

<?php
    echo $this->tag->form(
        [
            "casting/save",
            "autocomplete" => "off",
            "class" => "form-horizontal"
        ]
    );
?>


    <div class="clearfix">
        <label for="idpelicula">Pelicula</label>
        <?= $this->tag->select(['idpelicula', $peliculas, 'using' => ['id', 'nombre'], 'useEmpty' => true, 'emptyText' => 'Por favor, seleccione una opción', 'emptyValue' => '']) ?>
    </div>

    <div class="clearfix">
        <label for="idactor">Actor</label>
        <?= $this->tag->select(['idactor', $actores, 'using' => ['id', 'name'], 'useEmpty' => true, 'emptyText' => 'Por favor, seleccione una opción', 'emptyValue' => '']) ?>
    </div>

<?php echo $this->tag->hiddenField("id") ?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <?php echo $this->tag->submitButton(["Guardar", "class" => "btn btn-default"]) ?>
    </div>
</div>

<?php echo $this->tag->endForm(); ?>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>