<html>
 <head>
  <title>Bienvenido a Cinema</title>
  {{ stylesheet_link('css/bootstrap.theme.min.css') }}
  {{ javascript_include('js/bootstrap.min.js') }}
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px">{{ link_to("index", "Home", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("peliculas", "Peliculas", "class": "btn btn-primary") }}</li> <li style="margin: 10px">{{ link_to("actores", "Actores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("directores", "Directores", "class": "btn btn-primary") }}</li>  <li style="margin: 10px">{{ link_to("casting", "Casting", "class": "btn btn-primary") }}</li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

{{ form("casting/create", "autocomplete": "off") }}

<ul class="pager">
    <li class="btn btn-success">
        {{ link_to("casting", "&larr; Volver") }}
    </li>
    <li class="btn btn-success">
        {{ submit_button("Guardar", "class": "btn btn-success") }}
    </li>
</ul>

{{ content() }}

<div class="center scaffold">
    <h2>Guardar informacion de un nuevo Casting</h2>

    <div class="clearfix">
        <label for="idpelicula">Pelicula</label>
        {{ select('idpelicula', peliculas, 'using': ['id', 'nombre'],
        'useEmpty': true, 'emptyText': 'Por favor, seleccione una opción', 'emptyValue': '') }}
    </div>

    <div class="clearfix">
        <label for="idactor">Actor</label>
        {{ select('idactor', actores, 'using': ['id', 'lastname'],
        'useEmpty': true, 'emptyText': 'Por favor, seleccione una opción', 'emptyValue': '') }}
    </div>
    
</div>
</form>
<hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>