<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?= $this->getContent() ?>

<ul class="pager">
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['actores', '&larr; Volver', 'class' => 'btn btn-success']) ?>
    </li>
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['actores/new', 'Guardar informacion de un Nuevo Actor', 'class' => 'btn btn-success']) ?>
    </li>
</ul>
<br>
<?php $v109093277272641150511iterated = false; ?><?php $v109093277272641150511iterator = $page->items; $v109093277272641150511incr = 0; $v109093277272641150511loop = new stdClass(); $v109093277272641150511loop->self = &$v109093277272641150511loop; $v109093277272641150511loop->length = count($v109093277272641150511iterator); $v109093277272641150511loop->index = 1; $v109093277272641150511loop->index0 = 1; $v109093277272641150511loop->revindex = $v109093277272641150511loop->length; $v109093277272641150511loop->revindex0 = $v109093277272641150511loop->length - 1; ?><?php foreach ($v109093277272641150511iterator as $actores) { ?><?php $v109093277272641150511loop->first = ($v109093277272641150511incr == 0); $v109093277272641150511loop->index = $v109093277272641150511incr + 1; $v109093277272641150511loop->index0 = $v109093277272641150511incr; $v109093277272641150511loop->revindex = $v109093277272641150511loop->length - $v109093277272641150511incr; $v109093277272641150511loop->revindex0 = $v109093277272641150511loop->length - ($v109093277272641150511incr + 1); $v109093277272641150511loop->last = ($v109093277272641150511incr == ($v109093277272641150511loop->length - 1)); ?><?php $v109093277272641150511iterated = true; ?>
    <?php if ($v109093277272641150511loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Numero de Identificacion</th>
            <th>Nombre</th>
            <th>Apellido</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?= $actores->numid ?></td>
            <td><?= $actores->name ?></td>
            <td><?= $actores->lastname ?></td>
            <td width="7%"><?= $this->tag->linkTo(['actores/edit/' . $actores->id, '<i class="glyphicon glyphicon-edit"></i> Editar', 'class' => 'btn btn-default']) ?></td>
            <td width="7%"><?= $this->tag->linkTo(['actores/delete/' . $actores->id, '<i class="glyphicon glyphicon-remove"></i> Eliminar', 'class' => 'btn btn-default']) ?></td>
        </tr>
    <?php if ($v109093277272641150511loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="4" align="right">
                <div class="btn-group">
                    <?= $this->tag->linkTo(['actores/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['actores/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['actores/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['actores/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn']) ?>
                    <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
    <?php } ?>
<?php $v109093277272641150511incr++; } if (!$v109093277272641150511iterated) { ?>
    No se ha guardado ningun actor
<?php } ?>
   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>
