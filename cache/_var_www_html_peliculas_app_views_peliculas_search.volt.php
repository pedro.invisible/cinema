<html>
 <head>
  <title>Bienvenido a Cinema</title>
  <?= $this->tag->stylesheetLink('css/bootstrap.theme.min.css') ?>
  <?= $this->tag->javascriptInclude('js/bootstrap.min.js') ?>
 </head>
 <body>
 <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="container-fluid">
         <div class="navbar-header">
         <h1>Bienvenido a Cinema</h1>
             <p>Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas!</p>
             <ul class="nav">
               <li style="margin: 10px"><?= $this->tag->linkTo(['index', 'Home', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['peliculas', 'Peliculas', 'class' => 'btn btn-primary']) ?></li> <li style="margin: 10px"><?= $this->tag->linkTo(['actores', 'Actores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['directores', 'Directores', 'class' => 'btn btn-primary']) ?></li>  <li style="margin: 10px"><?= $this->tag->linkTo(['casting', 'Casting', 'class' => 'btn btn-primary']) ?></li>
             </ul>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="navbarCollapse">
             
         </div>
     </div>
 </nav>
<div class="container-fluid" style="margin-top:50px">
<?= $this->getContent() ?>

<ul class="pager">
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['peliculas', '&larr; Volver', 'class' => 'btn btn-success']) ?>
    </li>
    <li class="btn btn-success">
        <?= $this->tag->linkTo(['peliculas/new', 'Guardar informacion de una Nueva Pelicula', 'class' => 'btn btn-success']) ?>
    </li>
</ul>

<?php $v182736161960232771801iterated = false; ?><?php $v182736161960232771801iterator = $page->items; $v182736161960232771801incr = 0; $v182736161960232771801loop = new stdClass(); $v182736161960232771801loop->self = &$v182736161960232771801loop; $v182736161960232771801loop->length = count($v182736161960232771801iterator); $v182736161960232771801loop->index = 1; $v182736161960232771801loop->index0 = 1; $v182736161960232771801loop->revindex = $v182736161960232771801loop->length; $v182736161960232771801loop->revindex0 = $v182736161960232771801loop->length - 1; ?><?php foreach ($v182736161960232771801iterator as $peliculas) { ?><?php $v182736161960232771801loop->first = ($v182736161960232771801incr == 0); $v182736161960232771801loop->index = $v182736161960232771801incr + 1; $v182736161960232771801loop->index0 = $v182736161960232771801incr; $v182736161960232771801loop->revindex = $v182736161960232771801loop->length - $v182736161960232771801incr; $v182736161960232771801loop->revindex0 = $v182736161960232771801loop->length - ($v182736161960232771801incr + 1); $v182736161960232771801loop->last = ($v182736161960232771801incr == ($v182736161960232771801loop->length - 1)); ?><?php $v182736161960232771801iterated = true; ?>
    <?php if ($v182736161960232771801loop->first) { ?>
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Nombre de pelicula</th>
            <th>Año de estreno</th>
            <th>Director</th>
        </tr>
    </thead>
    <tbody>
    <?php } ?>
        <tr>
            <td><?= $peliculas->nombre ?></td>
            <td><?= $peliculas->year ?></td>
            <td><?= $peliculas->getDirectores()->name ?> <?= $peliculas->getDirectores()->lastname ?></td>
            <td width="7%"><?= $this->tag->linkTo(['peliculas/cast/' . $peliculas->id, '<i class="glyphicon glyphicon-remove"></i> Casting', 'class' => 'btn btn-default']) ?></td><td width="7%"><?= $this->tag->linkTo(['peliculas/edit/' . $peliculas->id, '<i class="glyphicon glyphicon-edit"></i> Edit', 'class' => 'btn btn-default']) ?></td>
            <td width="7%"><?= $this->tag->linkTo(['peliculas/delete/' . $peliculas->id, '<i class="glyphicon glyphicon-remove"></i> Delete', 'class' => 'btn btn-default']) ?></td>
        </tr>
    <?php if ($v182736161960232771801loop->last) { ?>
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    <?= $this->tag->linkTo(['peliculas/search', '<i class="icon-fast-backward"></i> First', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['peliculas/search?page=' . $page->before, '<i class="icon-step-backward"></i> Previous', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['peliculas/search?page=' . $page->next, '<i class="icon-step-forward"></i> Next', 'class' => 'btn']) ?>
                    <?= $this->tag->linkTo(['peliculas/search?page=' . $page->last, '<i class="icon-fast-forward"></i> Last', 'class' => 'btn']) ?>
                    <span class="help-inline"><?= $page->current ?> of <?= $page->total_pages ?></span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    <?php } ?>
<?php $v182736161960232771801incr++; } if (!$v182736161960232771801iterated) { ?>
    No products are recorded
<?php } ?>

   <hr>
    <div class="row">
        <div class="col-xs-12">
            <footer>
                <p>&copy; Cinema te permite llevar un registro de tus peliculas, los actores y directores asociados a estasy mucho mas! </p>
            </footer>
        </div>
    </div>
</div>