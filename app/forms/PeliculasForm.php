<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;

class PeliculasForm extends Form
{
    /**
     * Initialize the Peliculas form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $name = new Text("name");
        $name->setLabel("Nombre de la Pelicula");
        $name->setFilters(['striptags', 'string']);
        $name->addValidators([
            new PresenceOf([
                'message' => 'El Nombre de la Pelicula es un Campo Requerido'
            ])
        ]);
        $this->add($name);

        $iddirector = new Select('id', Directores::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'emptyText'  => '...',
            'emptyValue' => ''
        ]);
        $iddirector->setLabel('Director');
        $this->add($iddirector);


        $year = new Text("year");
        $year->setLabel("Año de estreno");
        $year->setFilters(['date_format(YYYY)']);
        $year>addValidators([
            new PresenceOf([
                'message' => 'El Año de estreno es un campo Requerido'
            ]),
            new Numericality([
                'message' => 'El Año de estreno es un campo Requerido'
            ])
        ]);
        $this->add($year);

    }
}
