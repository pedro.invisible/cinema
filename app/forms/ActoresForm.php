<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;

class ActoresForm extends Form
{
    /**
     * Initialize the products form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $numid = new Text("numid");
        $numid->setLabel("Numero de Identificacion");
        $numid->setFilters(['integer']);
        $numid->addValidators([
            new PresenceOf([
                'message' => 'El Numero de Identificacion es un campo Requerido'
            ])
        ]);
        $this->add($numid);


        $name = new Text("name");
        $name->setLabel("Nombre");
        $name->setFilters(['striptags', 'string']);
        $name->addValidators([
            new PresenceOf([
                'message' => 'El Nombre es un Campo Requerido'
            ])
        ]);
        $this->add($name);


        $lastname = new Text("lastname");
        $lastname->setLabel("Apellido");
        $lastname->setFilters(['striptags', 'string']);
        $lastname->addValidators([
            new PresenceOf([
                'message' => 'El Apellido es un campo Requerido'
            ])
        ]);
        $this->add($lastname);

    }
}
